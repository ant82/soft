<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'login'    => 'required|min:8|regex:/^[\w]+$/|unique:users',
            'password' => 'required|min:6|regex:/.*(?=.{6,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/|confirmed',
        ], [
            'login.required'    => 'Укажите логин',
            'login.regex'       => 'Только буквы и цифры',
            'login.min'         => 'Минимум :min символов',
            'login.unique'      => 'Такой логин уже зарегистрирован',
            'password.required' => 'Придумайте пароль',
            'password.regex'    => 'Обязательно символы в верхнем и нижнем регистрах + цифры',
            'password.min'      => 'Минимум :min символов']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {

        return User::create([
            'login'    => $data['login'],
            'name'     => $data['name'],
            'password' => bcrypt($data['password']),
        ]);
    }

}
