<?php

namespace App\Http\Controllers;

use App\Message;


class IndexController extends Controller
{
    /**
     * Обрабатывает запрос на показ главной
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //достаем все сообщения, упоряд. от новых к старым
        $messages = Message::orderBy('created_at', 'desc')->get();

        return view('index', ['messages' => $messages]);
    }
}
