<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Services\MessageService;


class MessageController extends Controller
{
    /**
     * @var MessageService
     */
    private $messageService;

    /**
     * MessageController constructor.
     * @param MessageService $messageService
     */
    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * Обработка запроса на добавление сообщения
     * @param MessageRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addMessage(MessageRequest $request)
    {
        $res = $this->messageService->addMessage($request);

        if (false === $res) {
            $errors = $this->messageService->getErrors();
            //работа с ошибками
        }

        return redirect('/');
    }
}
