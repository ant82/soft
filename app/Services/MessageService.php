<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 25.06.2017
 * Time: 12:17
 */

namespace App\Services;


use App\Http\Requests\MessageRequest;

class MessageService
{
    /**
     * Массив ошибок
     * @var array
     */
    private $errors = [];

    /**
     * Добавление сообщения
     * @param MessageRequest $messageRequest
     * @return bool
     */
    public function addMessage(MessageRequest $messageRequest)
    {
        $this->errors = [];
        try {
            $msg = \App\Message::create(['text' => $messageRequest->get('text'), 'author_id' => \Auth::getUser()->id]);
            $msg->save();
        } catch (\Exception $exception) {
            $this->errors[] = $exception->getMessage();

            return false;
        }

        return true;
    }

    /**
     * Возвращает массив ошибок
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}