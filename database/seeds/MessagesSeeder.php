<?php

use Illuminate\Database\Seeder;

class MessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for ($i = 1; $i <= 10; $i++) {
            DB::table('messages')->insert([
                'text'       => $faker->realText(),
                'created_at' => date("Y-m-d H:i:s", mt_rand(1, time())),
                'author_id'  => mt_rand(1, 10),
            ]);
        }
    }
}
