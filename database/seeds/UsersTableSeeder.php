<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for ($i = 1; $i <= 11; $i++) {
            DB::table('users')->insert([
                'login'      => str_random(mt_rand(8, 18)),
                'name'       => $faker->name,
                'created_at' => date('Y-m-d H:i:s'),
                'password'   => bcrypt(str_random(mt_rand(6, 18)) . mt_rand(0, 9999)),
            ]);
        }
    }
}
