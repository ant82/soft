@extends('layout')

@section('content')
    <div class="span4"></div>
    <div class="span8">

        <form method="post" class="form-horizontal" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="control-group">
                <b>Регистрация</b>
            </div>
            <div class="control-group @if ($errors->has('login'))error @endif">
                <input type="text" id="inputLogin" name="login" placeholder="Логин" data-cip-id="inputLogin"
                       value="{{ old('login') }}"
                       autocomplete="off">
                @if ($errors->has('login'))
                    <span class="help-inline">{{ $errors->first('login') }}</span>
                @endif
            </div>
            <div class="control-group  @if ($errors->has('password'))error @endif">
                <input type="password" id="inputPassword" name="password" placeholder="Пароль"
                       data-cip-id="inputPassword">
                @if ($errors->has('password'))
                    <span class="help-inline">{{ $errors->first('password') }}</span>
                @endif
            </div>
            <div class="control-group @if ($errors->has('password_confirmation'))error @endif">
                <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Повторите пароль"
                       data-cip-id="password_confirmation">
                @if ($errors->has('password_confirmation'))
                    <span class="help-inline">{{ $errors->first('password_confirmation') }}</span>
                @endif
            </div>
            <div class="control-group">
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </form>
    </div>
@endsection
