@extends('layout')

@section('content')
    <div class="span2"></div>
    <div class="span8">
        @if (\Illuminate\Support\Facades\Auth::check())
            <form action="{{route('add_message')}}" method="post" class="form-horizontal" style="margin-bottom: 50px;">
                {{ csrf_field() }}
                @if ($errors->has('text'))
                    <div class="alert alert-error">
                        Сообщение не может быть пустым
                    </div>
                @endif
                <div class="control-group">
                <textarea style="width: 100%; height: 50px;" name="text" id="inputText" placeholder="Ваше сообщение..."
                          data-cip-id="inputText"></textarea>
                </div>
                <div class="control-group">
                    <button type="submit" class="btn btn-primary">Отправить сообщение</button>
                </div>
            </form>
        @endif

        @foreach ($messages as $msg)
            <div class="well">
                <h5>{{$msg->author->name}}:</h5>
               {{$msg->text}}
                <div style="margin-top:10px;color: grey;font-size: 11px">{{$msg->created_at->format('d.m.Y H:i')}}</div>
            </div>
        @endforeach
    </div>
@endsection