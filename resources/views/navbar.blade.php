<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">Сайтсофт</a>
        <ul class="nav">
            <li class="active"><a href="{{route('index')}}">Главная</a></li>
            @if (\Illuminate\Support\Facades\Auth::check() == false)
                <li><a href="{{route('login')}}">Авторизация</a></li>
                <li><a href="{{route('register')}}">Регистрация</a></li>
            @endif
        </ul>

        @if (\Illuminate\Support\Facades\Auth::check())
            <ul class="nav pull-right">
                <li><a>{{\Illuminate\Support\Facades\Auth::getUser()->login}}</a></li>
                <li><a href="{{route('logout')}}">Выход</a></li>
            </ul>
        @endif
    </div>
</div>