<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Routing\Router;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddMessageTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var TestResponse
     */
    protected $response;


    public function setUp()
    {
        parent::setUp();
        $user = new User(array('id' => 1, 'login' => 'TestUser', 'name' => 'TestUserName'));
        $this->be($user);
        $this->response = $this->get(route('index'));
    }

    /**
     * Тест ошибки при пустом сообщении
     *
     * @return void
     */
    public function testEmptyMessageSend()
    {

        $this->response->assertSee(sprintf('action="%s"', route('add_message')));
        $postResponse = $this->post(route('add_message'), ['text' => '']);
        $postResponse->assertRedirect(route('index'));
        $postResponse->assertSessionHasErrors('text');
    }

    /**
     * Тест ошибки при пустом сообщении
     *
     * @return void
     */
    public function testAddMessage()
    {
        $user     = new User(array('login' => 'TestUser', 'name' => 'TestUserName'));
        $user->id = 1;
        $this->be($user);
        $this->post(route('add_message'), ['text' => 'This is a test message']);
        $this->assertDatabaseHas('messages', ['text' => 'This is a test message', 'author_id' => 1]);
    }


}
