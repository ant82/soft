<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Routing\Router;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IndexPageAuthTest extends TestCase
{

    /**
     * @var TestResponse
     */
    protected $response;


    public function setUp()
    {
        parent::setUp();
        $user = new User(array('login' => 'TestUser', 'name' => 'TestUserName'));
        $this->be($user);
        $this->response = $this->get('/');
    }


    /**
     * Тест ответа главной
     *
     * @return void
     */
    public function testIndexOkStatus()
    {
        $this->response->assertStatus(200);
    }

    /**
     * Тест наличия логаута
     *
     * @return void
     */
    public function testIndexLogout()
    {
        $this->response->assertSeeText('Выход');
    }

    /**
     * Тест отсутствия  аутентификации/регистрации
     *
     * @return void
     */
    public function testIndexNoAuthContent()
    {
        $this->response->assertDontSeeText('Авторизация');
        $this->response->assertDontSeeText('Регистрация');
        $this->response->assertDontSee(route('login'));
        $this->response->assertDontSee(route('register'));
    }

}
