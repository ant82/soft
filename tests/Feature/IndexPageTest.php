<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Routing\Router;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IndexPageTest extends TestCase
{

    /**
     * @var TestResponse
     */
    protected $response;


    public function setUp()
    {
        parent::setUp();
        $this->response = $this->get('/');
    }

    /**
     * Тест ответа главной
     *
     * @return void
     */
    public function testIndexOkStatus()
    {
        $this->response->assertStatus(200);
    }

    /**
     * Тест наличия  аутентификации/регистрации
     *
     * @return void
     */
    public function testIndexAuthContent()
    {
        $this->response->assertSeeText('Авторизация');
        $this->response->assertSeeText('Регистрация');
        $this->response->assertSee(route('login'));
        $this->response->assertSee(route('register'));
    }

    /**
     * Тест отсутствия логаута
     *
     * @return void
     */
    public function testIndexNoLogout()
    {
        $this->response->assertDontSeeText('Выход');
    }




}
