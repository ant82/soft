<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Routing\Router;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegisterPageTest extends TestCase
{

    /**
     * @var TestResponse
     */
    protected $response;


    public function setUp()
    {
        parent::setUp();

        $this->response = $this->get(route('register'));
    }

    /**
     * Тест ответа
     *
     * @return void
     */
    public function testOkStatus()
    {
        $this->response->assertStatus(200);

    }

    /**
     * Тест наличия аутентификации
     *
     * @return void
     */
    public function testAuthContent()
    {
        $this->response->assertSeeText('Регистрация');
        $this->response->assertSee(sprintf('action="%s"', route('register')));

    }


}
